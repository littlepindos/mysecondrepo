﻿using System.Data.OleDb;
using System.Data;
using System.Data.Common;

namespace ConsoleApp1
{
    class Game
    {
        public string name;
        public string genre;
        public string[] os;
        public string[] languages;
        public string developer;
    }
    class UseDB
    {
        OleDbConnection conn;
        DataSet ds = new DataSet();
        DbCommand comm;

        public UseDB(string conString)
        {
            conn = new OleDbConnection(conString);
        }

        public void OpenCon()
        {
            conn.Open();
        }

        public int Count()
        {
            int count;
            comm = conn.CreateCommand();
            comm.CommandText = "Select count(*) from games";
            count = (int)comm.ExecuteScalar();
            return count;
        }

        public DataTable AllGames()
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }

        public DataTable SearchByID(int id)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games where id=" + id;
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public DataTable SearchByName(string n)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games where name='" + n +"'";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public DataTable SearchByOS(string os)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games where os.value='" + os + "'";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public DataTable SearchByGenre(string g)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games where genre='" + g + "'";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public DataTable SearchByLanguage(string l)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            //throw new System.Exception("Select * from games where language.value='" + l + "'");
            comm.CommandText = "Select * from games where [language].value='" + l + "'";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public DataTable SearchByDeveloper(string d)
        {
            DbDataReader dr;
            DataTable temp = new DataTable();
            comm = conn.CreateCommand();
            comm.CommandText = "Select * from games where developer='" + d + "'";
            dr = comm.ExecuteReader();
            temp.Load(dr);
            dr.Close();
            return temp;
        }
        public int InsertGame(Game tempgame,int oscount,int langcount)
        {
            comm = conn.CreateCommand();
            comm.CommandText = "INSERT INTO games(Name,Genre,Developer) VALUES('" + tempgame.name + "','" + tempgame.genre + "','" + tempgame.developer + "');";
            int rows = comm.ExecuteNonQuery();
            for (int i = 0; i < oscount; i++)
            {
                comm.CommandText = "INSERT INTO games(OS.Value) VALUES('" + tempgame.os[i] + "') WHERE id=(select MAX(id) from games);";
                rows = comm.ExecuteNonQuery();
            }
            for (int i = 0; i < langcount; i++)
            {
                comm.CommandText = "INSERT INTO games(Language.Value) VALUES('" + tempgame.languages[i] + "') WHERE id=(select MAX(id) from games);";
                rows = comm.ExecuteNonQuery();
            }
            return rows;
        }

        public int DeleteGame(int id)
        {
            comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM games WHERE id=" + id;
            int rows = comm.ExecuteNonQuery();
            return rows;
        }

        public void CloseCon()
        {
            conn.Close();
        }

    }
}