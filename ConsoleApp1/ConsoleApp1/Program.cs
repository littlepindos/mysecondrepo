﻿using System;
using System.Xml;
using System.Data;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Task1()
        {
            DriveInfo dic = new DriveInfo("c:");
            DriveInfo did = new DriveInfo("d:");
            var a=dic.RootDirectory.GetDirectories();
            StreamWriter q = new StreamWriter("wwwa.txt");
            q.WriteLine("Info c:");
            q.WriteLine(dic.DriveType);
            q.WriteLine(dic.DriveFormat);
            q.WriteLine(dic.AvailableFreeSpace);
            q.WriteLine(dic.TotalSize - dic.AvailableFreeSpace);
            foreach (var i in a)
            {
                q.WriteLine(i.FullName);
            }
            q.WriteLine("Info d:");
            q.WriteLine(did.DriveType);
            q.WriteLine(did.DriveFormat);
            q.WriteLine(did.AvailableFreeSpace);
            q.WriteLine(did.TotalSize - did.AvailableFreeSpace);
            a = did.RootDirectory.GetDirectories();
            foreach (var i in a)
            {
                q.WriteLine(i.FullName);
            }
            q.WriteLine();
            q.Close();
            Console.ReadKey();
        }
        static void writetable(StreamWriter q,DataTable dataTable)
        {
            q.WriteLine("│    ID  │                   Name                  │            Genre             │                                  OS                                            │                                                             Language                                                                  │                   Developer               │\n");
            q.WriteLine("│======================================================================================================================================================================================================================================================================================================================================================│\n");
            foreach (DataRow dataRow in dataTable.Rows)
            {
                q.WriteLine("│ " + String.Format("{0,-7}", dataRow[0].ToString()) + "│ " + String.Format("{0,-40}", dataRow[1]) + "│ " + String.Format("{0,-29}", dataRow[2]) + "│ " + String.Format("{0,-79}", dataRow[3]) + "│ " + String.Format("{0,-134}", dataRow[4]) + "│ " + String.Format("{0,-42}", dataRow[5]) + "│");
                q.WriteLine("│──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────│\n");
            }
        }
        static void Task2()
        {
            StreamReader streamReader = new StreamReader("1.txt");
            string connString = streamReader.ReadLine();
            UseDB useDB = new UseDB(connString);
            useDB.OpenCon();
            DataTable dataTable = useDB.AllGames();
            StreamWriter q = new StreamWriter("waa.txt");
            writetable(q, dataTable);
            q.Close();
            string Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByID(Convert.ToInt16(Filter));
            q = new StreamWriter("f1.txt");
            writetable(q, dataTable);
            q.Close();
            Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByName(Filter);
            q = new StreamWriter("f2.txt");
            writetable(q, dataTable);
            q.Close();
            Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByGenre(Filter);
            q = new StreamWriter("f3.txt");
            writetable(q, dataTable);
            q.Close();
            Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByOS(Filter);
            q = new StreamWriter("f4.txt");
            writetable(q, dataTable);
            q.Close();
            Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByLanguage(Filter);
            q = new StreamWriter("f5.txt");
            writetable(q, dataTable);
            q.Close();
            Filter = streamReader.ReadLine();
            dataTable = useDB.SearchByDeveloper(Filter);
            q = new StreamWriter("f6.txt");
            writetable(q, dataTable);
            q.Close();
            streamReader = new StreamReader("waa.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f1.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f2.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f3.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f4.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f5.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            streamReader = new StreamReader("f6.txt");
            Console.WriteLine(streamReader.ReadToEnd());
            useDB.CloseCon();
            Console.ReadKey();
        }
        static void Xmltable(XmlTextWriter xmlTextWriter,DataTable dataTable)
        {
            xmlTextWriter.WriteStartElement("games");
            foreach (DataRow dataRow in dataTable.Rows)
            {
                xmlTextWriter.WriteStartElement("game");
                xmlTextWriter.WriteAttributeString("id", dataRow[0].ToString());
                xmlTextWriter.WriteAttributeString("name", dataRow[1].ToString());
                xmlTextWriter.WriteAttributeString("genre", dataRow[2].ToString());
                xmlTextWriter.WriteAttributeString("os", dataRow[3].ToString());
                xmlTextWriter.WriteAttributeString("language", dataRow[4].ToString());
                xmlTextWriter.WriteAttributeString("developer", dataRow[5].ToString());
                xmlTextWriter.WriteEndElement();
            }
            xmlTextWriter.WriteEndElement();
        }
        static void Consolexml(XmlDocument xmlDocument)
        {
            Console.WriteLine("│    ID  │                   Name                  │            Genre             │                                  OS                                            │                                                             Language                                                                  │                   Developer               │\n");
            Console.WriteLine("│======================================================================================================================================================================================================================================================================================================================================================│\n");
            foreach (XmlNode dataRow in xmlDocument.GetElementsByTagName("game"))
            {
                Console.WriteLine("│ " + String.Format("{0,-7}", dataRow.Attributes[0].Value.ToString()) + "│ " + String.Format("{0,-40}", dataRow.Attributes[1].Value.ToString()) + "│ " + String.Format("{0,-29}", dataRow.Attributes[2].Value.ToString()) + "│ " + String.Format("{0,-79}", dataRow.Attributes[3].Value.ToString()) + "│ " + String.Format("{0,-134}", dataRow.Attributes[4].Value.ToString()) + "│ " + String.Format("{0,-42}", dataRow.Attributes[5].Value.ToString()) + "│");
                Console.WriteLine("│──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────│\n");
            }
        }
        static void Task3()
        {
            XmlTextWriter xmlTextWriter = new XmlTextWriter("xml1.xml", null);
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("test.xml");
            string connString = xmlDocument.GetElementsByTagName("connString")[0].Attributes[0].Value.ToString();
            UseDB useDB = new UseDB(connString);
            useDB.OpenCon();
            DataTable dataTable = useDB.AllGames();
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            var mas = xmlDocument.GetElementsByTagName("filter");
            string filter = mas[0].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByID(Convert.ToInt16(filter));
            xmlTextWriter = new XmlTextWriter("f1.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            filter = mas[1].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByName(filter);
            xmlTextWriter = new XmlTextWriter("f2.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            filter = mas[2].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByGenre(filter);
            xmlTextWriter = new XmlTextWriter("f3.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            filter = mas[3].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByOS(filter);
            xmlTextWriter = new XmlTextWriter("f4.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            filter = mas[4].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByLanguage(filter);
            xmlTextWriter = new XmlTextWriter("f5.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            filter = mas[5].Attributes[0].Value.ToString();
            dataTable = useDB.SearchByDeveloper(filter);
            xmlTextWriter = new XmlTextWriter("f6.xml", null);
            Xmltable(xmlTextWriter, dataTable);
            xmlTextWriter.Close();
            xmlDocument.Load("xml1.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f1.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f2.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f3.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f4.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f5.xml");
            Consolexml(xmlDocument);
            xmlDocument.Load("f6.xml");
            Consolexml(xmlDocument);
            useDB.CloseCon();
            Console.ReadKey();
        }
        static void Main(string[] args)
        {
            Task1();
            Console.Clear();
            Task2();
            Console.Clear();
            Task3();
        }
    }
}
